# Njump for Drupal

Just like https://git.njump.me/njump this module will give you a static link to a Nostr event using the route `/e/{event_id}`.

### TODO's

- [ ] Show raw data of requested event.
- [ ] Render content
- [ ] Metatag integration for open graph metatags
- [ ] Process bech32 encoded references (NIP-19)
